﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    class DemeritPointsCalculatorTests
    {
        [Test]
        [TestCase(301)]
        [TestCase(-1)]
        public void CalculateDemeritPoints_SpeedIsLessThanZeroAndHigherThanSpeedLimit_ThrowArgumentOutOfRangeException(int speed)
        {
            var DemeritPoint = new DemeritPointsCalculator();

           
            Assert.That( () => DemeritPoint.CalculateDemeritPoints(speed), Throws.Exception.TypeOf<ArgumentOutOfRangeException>());
        }

        [Test]
        [TestCase(2)]
        [TestCase(65)]
        [TestCase(0)]
        public void CalculateDemeritPoints_SpeedIsHigherThanZeroAndLessThanSpeedLimit_Return0(int speed)
        {
            var DemeritPoint = new DemeritPointsCalculator();

            var result = DemeritPoint.CalculateDemeritPoints(speed);

            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        [TestCase(300,47)]
        [TestCase(64, 0)]
        [TestCase(65, 0)]
        [TestCase(70, 1)]
        [TestCase(75, 2)]
        public void CalculateDemeritPoints_SpeedIsHigherThanSpeedLimitAndLessThanMaxLimit_ReturnAmountOfDemeritPoints(int speed, int expectedResult)
        {
            var DemeritPoint = new DemeritPointsCalculator();

            var result = DemeritPoint.CalculateDemeritPoints(speed);
          

            Assert.That(result, Is.EqualTo(expectedResult));
        }
    }
}
