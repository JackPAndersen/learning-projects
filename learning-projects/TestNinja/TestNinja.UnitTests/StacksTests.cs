﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    class StacksTests
    {
        [Test]
        public void Push_ArgIsNull_ThrowArgumentNullException()
        {
            var stack = new Fundamentals.Stack<string>();

            Assert.That(() => stack.Push(null), Throws.ArgumentNullException);

        }

        [Test]
        public void Push_ValidArg_AddItemToObject()
        {
            var stack = new Fundamentals.Stack<string>();

            stack.Push("a");

            Assert.That(stack.Count, Is.EqualTo(1));

        }

        [Test]
        public void Count_EmpyStack_ReturnZero()
        {
            var stack = new Fundamentals.Stack<String>();

            Assert.That(stack.Count, Is.EqualTo(0));
        }

        [Test]
        public void Pop_ArgIsNull_ThrowArgumentNullException()
        {
            var stack = new Fundamentals.Stack<string>();

            Assert.That(() => stack.Pop(), Throws.InvalidOperationException);
        }

        [Test]
        public void Pop_StackWithAFewObjects_ReturnObjectOnTop()
        {
            var stack = new Fundamentals.Stack<String>();

            stack.Push("a");
            stack.Push("b");
            stack.Push("c");

            var result = stack.Pop();

            Assert.That(result, Is.EqualTo("c"));
        }

        [Test]
        public void Pop_ArgIsValid_RemoveObjectFromTheStack()
        {
            var stack = new Fundamentals.Stack<string>();

            stack.Push("a");
            stack.Pop();

            Assert.That(stack.Count, Is.EqualTo(0));
        }

        [Test]
        public void Peek_EmptyStackWithNoObjects_ThrowInvalidOperationException()
        {
            var stack = new Fundamentals.Stack<string>();

            Assert.That(() => stack.Peek(), Throws.InvalidOperationException);

        }
        [Test]
        public void Peek_StackWithObjects_ReturnObjectOnTopOfTheStack()
        {
            var stack = new Fundamentals.Stack<string>();

            stack.Push("a");
            stack.Push("b");
            stack.Push("c");

            var result = stack.Peek();

            Assert.That(result, Is.EqualTo("c"));

        }

        [Test]
        public void Peek_StackWithObjects_DoesNotRemoveTheObjectOnTopOfTheStack()
        {
            var stack = new Fundamentals.Stack<String>();

            stack.Push("a");
            stack.Push("b");
            stack.Push("c");

            Assert.That(stack.Count, Is.EqualTo(3));
            
        }
    }
    
}
