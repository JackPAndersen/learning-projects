﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBuilderExercise
{
    

    public class Field
    {
        public string Type, Name;

        public override string ToString()
        {
            return $"public {Type} {Name}";
        }
    }

    class FieldInfo
    {
        public string Name;
        public List<Field> fields = new List<Field>();

        public FieldInfo()
        {

        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"public class {Name}").AppendLine("{");
            foreach (var f in fields)
            {
                sb.AppendLine($"   {f};");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }
    }

    public class CodeBuilder
    {
        private readonly string rootName;
        Field root = new Field();
        public CodeBuilder(string rootName)
        {
            field.Name = rootName;
        }

        public CodeBuilder AddField(string name, string type)
        {
            field.fields.Add(new Field { Name = name, Type = type });
            return this;
        }

        public override string ToString()
        {
            return field.ToString();
        }

        private FieldInfo field = new FieldInfo();
    }

    class Program
    {
        static void Main(string[] args)
        {
            var cb = new CodeBuilder("Person").AddField("Name", "string").AddField("Age", "int");
            Console.WriteLine(cb);
            Console.ReadKey();
        }
    }
}
