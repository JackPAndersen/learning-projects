import socket

def server():
    port = 60003
    sockL = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sockL.bind( ("", port) )
    sockL.listen(2)
    (sockC, addr) = sockL.accept()
    return sockC

def client(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    return sock

ans = ""
while ans not in ["S","K"]:
    ans = input("Do you want to be the Server or Client (S/K): ")
    if (ans == "S"):
        sock = server()
    elif( ans == "K"):
        cHost = (input("What is the Host IP? : "))
        cPort = int(input("What is the Host Port?: "))
        sock = client(cHost,cPort)
    else: 
        print("Wrong Input. Please try again")

score = [0,0]
while True:

     userInput = input("{} Rock Paper or Scissors?( O for rock, U for paper and X for scissors): ".format(score))
     while userInput not in ["O","U","X"]:
          userInput = input("{} Rock Paper or Scissors?( O for rock, U for paper and X for scissors): ".format(score))

     barr = bytearray(userInput,"ASCII")
     sock.send(barr)
     barr2 = sock.recv(1024)
     recieved = barr2.decode("ASCII")

     if userInput is recieved:
        print("Draw! Nobody recieves a point")

     elif userInput is "X" and recieved is "U":
        score[0] += 1
     elif userInput is "O" and recieved is "X":
         score[0] += 1
     elif userInput is "U" and recieved is "O":
         score[0] += 1
     else:
         score[1] += 1

     print("{} Your opponent choose: {}".format(score,recieved))

     if score[0] is 10:
         print("You won by {} against {}".format(score[0], score[1]))
         sock.close()
     if score[1] is 10:
         print("You lost by {} against {}".format(score[1], score[0]))
         sock.close()


