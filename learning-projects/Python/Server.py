import socket
port = 60003
sockL = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sockL.bind( ("", port) )
sockL.listen(2)
while True:
    (sockC, addr) = sockL.accept()
    print( "uppkoppling från {}".format(addr) )
    while True:
        data = sockC.recv(1024)
        if not data:
            break
        sockC.send( data )
        print( data )
    sockC.close()
    print( "klienten {} nedkopplad".format(addr) )
