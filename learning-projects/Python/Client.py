
import socket
HOST = '192.168.0.142'
PORT = 60003
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((HOST, PORT))
sock.sendall(b'Hello, world')
data = sock.recv(1024)
sock.close()
print('Received', data)
